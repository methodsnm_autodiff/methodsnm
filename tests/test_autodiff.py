import import_hack
import numpy as np
from methodsnm.diff import *
import pytest

def test_add():
    a = AD(1., np.array([1,0]))
    b = AD(2., np.array([0,1]))
    b_int = 2

    assert AD(3., np.array([1,1])) == a + b
    assert AD(3., np.array([1,0])) == a + b_int

def test_radd():
    a = AD(1., np.array([1,0]))
    b = AD(2., np.array([0,1]))
    b_int = 2

    assert AD(3., np.array([1,1])) == b + a
    assert AD(3., np.array([1,0])) == b_int + a

def test_sub_1d():
    a = AD(1., np.array([1]))
    b = AD(2., np.array([1]))
    b_int = 2.

    assert AD(-1., np.array([0])) == a - b
    assert AD(-1., np.array([1])) == a - b_int
    
def test_rsub_1d():
    a = AD(1., np.array([1]))
    b = AD(2., np.array([1]))
    b_int = 2.

    assert AD(1., np.array([0])) == b - a
    assert AD(1., np.array([-1])) == b_int - a

def test_pow_1d():
    a = AD(3., np.array([ 1.]))
    a_invalid = AD(-1., np.array([-1.]))
    
    with pytest.raises(ValueError) as excinfo:  
        a_invalid**(-1)

    assert AD(1./3., np.array([np.log(1.)])) == a**(-1)
    assert str(excinfo.value) == "Invalid value of derivative. Must be positive."
    assert AD(1., np.array([ 0.])) == a ** 0
    assert a == a ** 1
    assert AD(9., np.array([ 2. * 3.])) == a ** 2

def test_pow_1d_more():
    a = AD(3., np.array([ 1.]))
    b = a + a + a

    assert AD(1., np.array([ 0.])) == b ** 0
    assert b == b ** 1
    # (b**2)' == ((3a)**2)' == 2*(3a)*3
    assert AD(81., np.array([2. * 3. *3. * 3.])) == b ** 2

def test_pow_2d():
    x = AD(1., np.array([1,0]))
    y = AD(2., np.array([0,1]))
    y_invalid = AD(2., np.array([0,-1]))

    f = x+y
    f_invalid = x+y_invalid
    
    with pytest.raises(ValueError) as excinfo:  
        f_invalid**(-1)

    assert str(excinfo.value) == "Invalid value of derivative. Must be positive."
    assert AD(1./3., np.array([np.log(1), np.log(1)])) == f**(-1)
    assert AD(1., np.array([0., 0.])) == f**0
    assert f == f**1
    assert AD(9., np.array([6., 6.])) == f**2


def test_mul_1d():
    a = AD(1., np.array([1.]))
    b = AD(2., np.array([1.]))
    b_int = 2

    assert AD(2., np.array([3.])) == a * b
    assert AD(2., np.array([2.])) == a * b_int
    
def test_rmul_1d():
    a = AD(1., np.array([1.]))
    b = AD(2., np.array([1.]))
    b_int = 2

    assert AD(2., np.array([3.])) == b * a
    assert AD(2., np.array([2.])) == b_int * a


def test_mul_2d():
    a = AD(1., np.array([1,0]))
    b = AD(2., np.array([0,1]))
    b_int = 2

    assert AD(2., np.array([2,1])) == a * b
    assert AD(2., np.array([2,0])) == a * b_int
    
def test_rmul_2d():
    a = AD(1., np.array([1,0]))
    b = AD(2., np.array([0,1]))
    b_int = 2

    assert AD(2., np.array([2,1])) == b * a
    assert AD(2., np.array([2,0])) == b_int * a
    
    
def test_div_1d():
    a = AD(1., np.array([1.]))
    b = AD(2., np.array([1.]))
    b_int = 2.
    b_invalid = AD(0., np.array([1.]))
    
    with pytest.raises(ZeroDivisionError) as excinfo:  
        a / b_invalid

    assert AD(1./2., np.array([1./4.])) == a / b
    assert AD(1./2., np.array([1./2.])) == a / b_int
    assert str(excinfo.value) == "You can't divide by zero!"
    
def test_rdiv_1d():
    a = AD(1., np.array([1.]))
    b = AD(2., np.array([1.]))
    b_int = 2.
    a_invalid = AD(0., np.array([1.]))
    
    with pytest.raises(ZeroDivisionError) as excinfo:  
        b / a_invalid

    assert AD(2., np.array([-1.])) == b / a
    assert AD(2., np.array([-2.])) == b_int / a
    assert str(excinfo.value) == "You can't divide by zero!"


def test_div_2d():
    a = AD(1., np.array([1,0]))
    b = AD(2., np.array([0,1]))
    b_int = 2.
    b_invalid = AD(0., np.array([0,1]))
    
    with pytest.raises(ZeroDivisionError) as excinfo:  
        a / b_invalid

    assert AD(1./2., np.array([1./2.,-1./4.])) == a / b
    assert AD(1./2., np.array([1./2.,0])) == a / b_int
    assert str(excinfo.value) == "You can't divide by zero!"


def test_rdiv_2d():
    a = AD(1., np.array([1,0]))
    b = AD(2., np.array([0,1]))
    b_int = 2.
    a_invalid = AD(0., np.array([1,0]))

    with pytest.raises(ZeroDivisionError) as excinfo:  
        b / a_invalid
        
    assert AD(2., np.array([-2.,1.])) == b / a
    assert AD(2., np.array([-2.,0])) == b_int / a
    assert str(excinfo.value) == "You can't divide by zero!"

