{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# A crash course on the Finite Element Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This document is meant as a **quick start** into the FEM. \n",
    "\n",
    "The idea here is to give a motivation and picture of the overall working principle of the method itself. \n",
    "\n",
    "The analysis of the method will require a more careful treatment (see e.g. NPDE 1). We concentrate on the conceptional aspects of the method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Three examples (in [`NGSolve`](https://ngsolve.org/)):\n",
    "\n",
    "* [Navier-Stokes equations (flow fields in fluids)](https://docu.ngsolve.org/latest/i-tutorials/wta/navierstokes.html)\n",
    "* [Maxwell's equations (electromagnetism)](https://docu.ngsolve.org/latest/i-tutorials/wta/coil.html)\n",
    "* [Elasticity (mechanics, structure deformation)](https://docu.ngsolve.org/latest/i-tutorials/wta/elasticity.html)\n",
    "\n",
    "What we saw (in short) are: \n",
    "* comparibly simple models for complex physical phenomena\n",
    "* the unknowns are fields (functions) defined on a (possibly complex) domain $\\Omega$\n",
    "* the equations are partial differential equations (PDEs)\n",
    "\n",
    "Many practical problems are even more complex due to e.g.\n",
    "* more complex geometries\n",
    "* more complex physics\n",
    "* non-linearities\n",
    "* stochasticity\n",
    "* coupling of several PDEs\n",
    "* ...\n",
    "\n",
    "We will however take a step back on consider rather simple problems:\n",
    "* linear PDEs\n",
    "* scalar unknowns\n",
    "* 1D / 2D\n",
    "\n",
    "But for these, we want to develop/understand solvers - more or less - from scratch."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Solving the Poisson equation\n",
    "\n",
    "We search for the solution $u: \\Omega \\to \\mathbb{R}$, so that\n",
    "$$\n",
    "-\\Delta u(x) = f(x) \\quad \\forall \\, x \\in \\Omega\n",
    "$$\n",
    "\n",
    "* $f: \\Omega \\to \\mathbb{R}$ is a given function\n",
    "* the domain $\\Omega$ (open, bounded, typically Lipschitz boundary or smoother) is a subset of $\\mathbb{R}^n$. \n",
    "* The Poisson equation is a model for many physical phenomena:\n",
    "  * f can be a heat source distribution, and u is the temperature\n",
    "  * f can be a electrical charge distribution, and u is the electrostatic potential\n",
    "* To select a unique solution $u$ we have to specify boundary conditions, for example homogeneous Dirichlet boundary conditions\n",
    "$$\n",
    "u(x) = 0 \\quad \\forall \\, x \\in \\partial \\Omega.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Quick motivation of the Poisson equation\n",
    "\n",
    "Let $V$ be a *control* volume in $\\Omega$ and let $S$ be the surface of $V$.\n",
    "Then a simple model for the heat flow out of $V$ is given by the energy balance:\n",
    "\n",
    "\"The change of heat energy in $V$ is given by the heat flux flowing in through $S$ plus the energy transfered into $V$ due to a heat source $f: \\Omega \\to \\mathbb{R}$.\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "If we further assume that the heat distribution is stationary, i.e. the heat energy in $V$ does not change in time, we obtain\n",
    "\n",
    "\"The ~~change of heat energy in $V$ is given by the~~ heat flux $\\mathbf{q}: S \\to \\mathbb{R}^d$ flowing in through $S$ plus the energy transfered into $V$ due to a heat source $f: \\Omega \\to \\mathbb{R}$ **is zero**.\"\n",
    "\n",
    "$$\n",
    "\\int_{\\partial V} -\\mathbf{q} \\cdot n \\, ds + \\int_V f \\, dx = 0. \\tag{B}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "A classical model for the heat flux is given by Fourier's law:\n",
    "$$\n",
    "\\mathbf{q} = - \\lambda \\nabla u,\n",
    "$$\n",
    "where $\\lambda$ is the thermal conductivity and $u$ is the temperature (or $\\lambda$ is the diffusivity and $u$ the concentration for other diffusion processes).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Then the divergence theorem allows us to transfer the surface integral into a volume integral:\n",
    "$$\n",
    "\\int_{\\partial V} -\\mathbf{q} \\cdot n \\, ds  = \\int_{\\partial V} \\lambda \\nabla u \\cdot n \\, ds = \\int_V \\operatorname{div}( \\lambda \\nabla u ) \\stackrel{\\lambda = \\text{const}}{=} \\int_V \\lambda \\Delta u \\, dx.\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Plugging this into $(B)$ we obtain\n",
    "$$\n",
    "    \\int_V \\lambda \\Delta u + f \\, dx = 0.\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As $V$ is arbitrary, we can (formally) localise the equation to obtain the PDE: Find $u: \\Omega \\to \\mathbb{R}$ so that **for all $x \\in \\Omega$** there holds\n",
    "$$\n",
    "-\\operatorname{div}(\\lambda \\nabla u) = - \\lambda \\Delta u= f. \\tag{P}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On the domain boundary we further need to specify on each part of the boundary conditions, either\n",
    "* the value of $u$, (Dirichlet boundary conditions)\n",
    "* the *flux* $\\mathbf{q}\\cdot n$ (Neumann boundary conditions) or\n",
    "* a more complex boundary condition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The simplest examples are *homogeneous* conditions, i.e. either $u=0$ or $\\mathbf{q} \\cdot n = 0$ and $\\lambda = 1$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Why/how to come up with discretization methods for PDEs?\n",
    "\n",
    "* The unknown is a function $u: \\Omega \\to \\mathbb{R}$, i.e. an infinite dimensional object\n",
    "* $(P)$ is formulated **for all $x \\in \\Omega$**, i.e. it is an infinite number of equations\n",
    "* To solve $(P)$ on a computer we need to discretize the problem, i.e. replace the infinite dimensional problem by a finite dimensional one\n",
    "* Three options are:\n",
    "  * Finite difference methods. Take only a finite number of points in $\\Omega$ and replace the derivatives by finite difference quotients\n",
    "  * Finite volume methods. Take only a finite number of (discrete) control volumes $V$ and replace the integrals by discrete (finite volume) approximations\n",
    "  * Finite element method: Replace the problem by a formultaion w.r.t. test function, i.e. formulate an equation **for all test functions** $\\phi$ in a space $V$. Afterwards replace $V$ by a finite dimensional subspace $V_h$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Special case: 1D, $\\lambda =1$\n",
    "\n",
    "On the interval $\\Omega = (a,b)$ the Poisson equation reads: Find $u:(a,b) \\to \\mathbb{R}$ so that\n",
    "$$\n",
    "- \\frac{d^2}{dx^2} u(x) = - u''(x) = f(x) \\quad \\forall \\, x \\in (a,b)\n",
    "$$\n",
    "with boundary conditions:\n",
    "* $u(a) = u(b) = 0$ (homogeneous Dirichlet boundary conditions) or\n",
    "* $\\frac{\\partial u}{\\partial n}(a) = \\frac{\\partial u}{\\partial n}(b) = 0$ (homogeneous Neumann boundary conditions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Weak formulation\n",
    "\n",
    "* The formulation above in $(P)$ is called the strong form of the Poisson PDE. \n",
    "* We will take a look at the weak form as the starting point for the finite element discretization method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* We multiply the Poisson equation by a so called test function. It is an essentially arbitrary function (some restriction will be given later as needed):\n",
    "$$\n",
    "- \\Delta u(x) v(x) = f(x) v(x) \\qquad \\forall x \\in \\Omega\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    " * We integrate over the domain $\\Omega$:\n",
    "\n",
    "$$\n",
    "- \\int_\\Omega \\Delta u(x) v(x) dx = \\int_\\Omega f(x) v(x) dx\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* From Gauss' Theorem applied to the vector field $\\nabla u v$ we obtain\n",
    "$$\n",
    "\\int_{\\partial \\Omega} \\nabla u \\cdot n \\, v \\,dx= \\int_\\Omega \\operatorname{div} (\\nabla u v) \\,dx\n",
    "= \\int_{\\Omega} \\Delta u v + \\nabla u \\nabla v \\,dx\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This allows to rewrite the left hand side such that (for suff. smooth fcts.)\n",
    "$$\n",
    "\\int_\\Omega \\nabla u \\nabla v - \\int_{\\partial \\Omega} \\frac{\\partial u}{\\partial n} v = \\int_\\Omega f v\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the case of Dirichlet boundary conditions we allow only test-functions $v$ such that $v(x) = 0$ on the boundary $\\partial \\Omega$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have derived the weak form: find $u$ such that $u = 0$ on $\\partial \\Omega$ and \n",
    "\n",
    "$$\n",
    "\\int_\\Omega \\nabla u \\nabla v = \\int_\\Omega f v \\tag{W}\n",
    "$$\n",
    "\n",
    "holds true for all test-functions $v$ with $v = 0$ on $\\partial \\Omega$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Note that the **weak** formulation needs only **first order derivatives** of $u$ and $v$ (and also only in $L^2$), in contrast to the strong form which requires second order derivatives of $u$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the right regularity to ask for $u$ and $v$?\n",
    "\n",
    "(Without further details) the right (*Sobolev*) space is:\n",
    "$$\n",
    "V = H^1(\\Omega) := \\{ u \\in L_2(\\Omega) : \\nabla u \\in L_2(\\Omega) \\},\n",
    "$$\n",
    "or, depending on the boundary conditions:\n",
    "$$\n",
    "V = H_0^1(\\Omega) = \\{ u \\in H^1(\\Omega) : u_{|\\partial \\Omega} = 0 \\}.\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us consider the term on the left hand side of the variational formulation:\n",
    "\n",
    "$$\n",
    "A(u,v) := \\int_{\\Omega} \\nabla u \\nabla v  \\,dx\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Given $u$ and $v$ from the Sobolev space, we can compute a number $\\int \\nabla u \\nabla v$\n",
    "* $A(.,.)$ is a function mapping from two elements from $V$ into $\\mathbb{R}$:\n",
    "\n",
    "$$\n",
    "A(.,.) : V \\times V \\rightarrow \\mathbb{R}\n",
    "$$\n",
    "\n",
    "The function $A(.,.)$ is linear in both arguments, and thus we call it a bilinear form."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, the right hand side\n",
    "\n",
    "$$\n",
    "f(v) := \\int_{\\Omega} f v \\,dx\n",
    "$$\n",
    "\n",
    "is a linear function\n",
    "\n",
    "$$\n",
    "f(\\cdot) : V \\rightarrow \\mathbb{R},\n",
    "$$\n",
    "\n",
    "which we call a linear form."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having these objects defined, our weak formulation reads now \n",
    "\n",
    "$$\n",
    "\\text{find} \\, u \\in V : \\quad A(u,v) = f(v) \\quad \\forall \\, v \\in V\n",
    "$$\n",
    "\n",
    "This abstract formalism of Hilbert spaces, bilinear and linear forms apply for a large class of (elliptic, but not only) partial differential equations.\n",
    "\n",
    "We now have a achieved an equation **for all test functions** $v \\in V$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Finite Element Method\n",
    "We cannot compute the solution in an infinite dimensional Hilbert space.\n",
    "But, we can define a finite dimensional sub-space (Galerkin method):\n",
    "$$\n",
    "V_h \\subset H^1_0\n",
    "$$\n",
    "\n",
    "and restrict the weak formulation to $V_h$:\n",
    "\n",
    "$$\n",
    "\\text{find} \\, u_h \\in V_h : \\quad A(u_h,v_h) = f(v_h) \\quad \\forall \\, v_h \\in V_h\n",
    "$$\n",
    "\n",
    "The finite element solution $u_h$ is the *Galerkin approximation* to the true solution $u$. \n",
    "\n",
    "One can analyze the $discretization error $\\| u - u_h \\|_{H^1}$ and in the case of the Poisson problem bound it by constant times the best approximation in $V_h$ (in the $H^1$ norm): \n",
    "\n",
    "$$\\| u - u_h \\|_{H^1} \\leq C \\inf_{v_h \\in V_h} \\| u - v_h \\|_{H^1} \\leq c N^{-k/n}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For computing the solution $u_h$ we have to choose a basis for the function space $V_h$, where $N = \\operatorname{dim} V_h$\n",
    "\n",
    "$$\n",
    "V_h = \\operatorname{span} \\{ \\phi_1(x), \\ldots \\phi_N(x) \\}\n",
    "$$\n",
    "\n",
    "By means of this basis we can expand the solution $u_h$ as\n",
    "\n",
    "$$\n",
    "u_h(x) = \\sum_{i=1}^N u_i \\phi_i(x)\n",
    "$$\n",
    "\n",
    "$u_i$ are the *degrees of freedoms* in the FE approximation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The coefficients $u_i$ are combined to the coefficient vector $u = (u_1, \\ldots, u_N) \\in \\mathbb{R}^N$\n",
    "\n",
    "Instead of testing with all test-functions from $V_h$, by linearity of $A(\\cdot,\\cdot)$ and $f(\\cdot)$, it is enough to test only with the basis functions $\\phi_j(x), j = 1, \\ldots, N$\n",
    "\n",
    "Thus, the finite element probem can be rewritten as\n",
    "\n",
    "$$\n",
    "\\text{find } u \\in \\mathbb{R}^N : \\quad A(\\sum_i u_i \\phi_i(x), \\phi_j(x)) = f(\\phi_j(x)) \\qquad \\forall \\, j = 1, \\ldots N\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By linearity of $A(\\cdot,\\cdot)$ in the first argument we can write\n",
    "\n",
    "$$\n",
    "\\text{find } u \\in \\mathbb{R}^N : \\quad \\sum_{i=1}^N A(\\phi_i, \\phi_j) \\, u_i = f(\\phi_j) \\qquad \\forall \\, j = 1, \\ldots N\n",
    "$$\n",
    "\n",
    "Since the basis functions are known, we can compute the matrix $A \\in \\mathbb{R}^{N\\times N}$ with entries\n",
    "\n",
    "$$\n",
    "A_{j,i} = A(\\phi_j,\\phi_i) = \\int_\\Omega \\nabla \\phi_j(x) \\cdot \\nabla \\phi_i(x)\n",
    "$$\n",
    "\n",
    "\n",
    "and the vector $f \\in \\mathbb{R}^N$ as\n",
    "\n",
    "$$\n",
    "f_j = f(\\phi_j) = \\int_\\Omega f(x) \\phi_j(x)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solving the finite element problem results in the linear system of equations for the coefficient vector $u = (u_1, \\ldots, u_N)$:\n",
    "\n",
    "$$\n",
    "\\text{find } u \\in \\mathbb{R}^N : \\quad A u = f\n",
    "$$\n",
    "\n",
    "By means of the coefficient vector, we have a representation of the finite element solution \n",
    "\n",
    "$$\n",
    "u_h(x) = \\sum u_i \\phi_i(x)\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Questions that remain for later:\n",
    "\n",
    "* How to setup the space $V_h$, respecitvely the basis functions $\\phi_i$?\n",
    "* How to then evaluate the integrals for $A_{j,i}$ and $f_j$?\n",
    "* How to solve the linear system $A u = f$ (efficiently)?\n",
    "\n",
    "$\\leadsto$ [quick FEM example](https://docu.ngsolve.org/latest/i-tutorials/wta/poisson.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task:\n",
    "\n",
    "We are given the 1D PDE: Find $u: (0,1) \\to \\mathbb{R}$ such that\n",
    "\\begin{align*}\n",
    "  u(x) + \\partial_x u(x) - \\partial_x^2 u(x) = u(x) + u'(x) - u''(x) & = f(x) \\quad \\text{ in } (0,1) , \\\\\n",
    "                  u(s) & = 0 \\quad \\text{ for } s \\in \\{0,1\\}.\n",
    "\\end{align*}\n",
    "for a given function $f: (0,1) \\to \\mathbb{R}$.\n",
    "\n",
    "* Formulate the corresponding weak form, i.e. find $u \\in H^1_0((0,1))$ such that\n",
    "$$\n",
    "  .. \n",
    "$$\n",
    "\n",
    "* Extract the bilinear and linear forms $A(\\cdot,\\cdot)$ and $f(\\cdot)$ from the weak form."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.3"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
