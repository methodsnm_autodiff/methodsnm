# Methodsnm Autodiff

## What is Autodiff?

We have used numerical differentiation as a fall back to compute the derivatives of shape functions. Alternatively we can directly compute the derivatives alongside the shapes itself using an autodiff data type. 

To this end we need to introduce a datatype which re-implements standard arithmetic operations. It holds a value and a derivative value. The arithmetic operations then compute value and derivative using standard rules of differentiation, e.g. product rule:
```python
x = AD(ip[0],(1,0)) # the expression corresponding to x has value ip[0] and derivative (1,0)
y = AD(ip[1],(0,1)) # the expression corresponding to x has value ip[0] and derivative (1,0)
z = x*y # the expression corresponding to z
# z.val == x.val * y.val
# z.der == x.val * y.der + x.der * y.val
```


## The AD class

- replacement for `float` type with an additional input of the local value of a derivative
- can do arithmetic operations: `+,-,*,/,**`
    - computes the value of the operation
    - computes the derivative on those operations

### Differentiation Rules

$$
u,v: \mathbb{R}^n \rightarrow \mathbb{R}, \\
k \in \mathbb{N}
$$

Addition Rule:
$$
\nabla(u+v) = \nabla u + \nabla v
$$

Subtraction Rule:
$$
\nabla(u-v) = \nabla u - \nabla v
$$


Multiplication Rule:
$$
\nabla (u \cdot v) = (\nabla u) \cdot v + u \cdot (\nabla v)
$$


Division Rule:
$$
\nabla\left(\frac{u}{v}\right) = \frac{(\nabla u) \cdot v - u \cdot (\nabla v)}{v^2}
$$

Power Rule:
$$
\nabla(u^n) = n \cdot u^{n-1} \cdot \nabla u
$$

## Implementation

- see `diff.py`.
- see also the [python documentation](https://docs.python.org/3/reference/datamodel.html#emulating-numeric-types) for operator overloading

### A note on FE implementations

- some FEs were implemented with only `float` in mind
- we altered the implementation, such that it also supports our `AD` type

### Tests

- we implemented comprehensive tests in `tests/test_autodiff.py` for the `AD` type
- we implemented comprehensive tests in `tests/test_fe_1d.py` and `tests/test_fe_2d.py` for the use of the `AD` type in the FE implementations
- we use Gitlab CI to publish a [test coverage report](https://methodsnm-methodsnm-autodiff-d00ca280b71459e4492ef31f85317a8798.pages.gwdg.de/htmlcov/)


