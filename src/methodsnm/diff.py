from typing import Tuple, Type, Union
import numpy as np

class AD:
    """
    This class represents a datatype that can automatically generate a derivative for a
    composition of supplied founding elements
    """
    def __init__(self, val: float, deriv: np.ndarray) -> None:
        """
        val: array_like, single value or numpy array, that holds one ore moe values for the element
        deriv: array_like, holds the derivative of the element
        """
        self.val = val
        self.deriv = deriv

    def __add__(self, other: Union[float, 'AD']) -> 'AD':
        """
        returns self + other
        """
        if isinstance(other, float) or isinstance(other, int):
            return AD(self.val + other, self.deriv)
        return AD(self.val + other.val, self.deriv + other.deriv)

    def __radd__(self, other: float) -> 'AD':
        """
        returns other + self
        """
        return AD(other + self.val, self.deriv)
    
    def __sub__(self, other: Union[float, 'AD']) -> 'AD':
        """
        returns self - other
        """
        if isinstance(other, AD):
            return AD(self.val - other.val, self.deriv - other.deriv)
        return AD(self.val - other, self.deriv)
        #raise NotImplementedError(f"not implemented for other: {type(other.dtype)}")

    def __rsub__(self, other: float) -> 'AD':
        """
        returns other - self
        """
        return AD(other - self.val, -self.deriv)
    
    def __mul__(self, other: Union[float, 'AD']) -> 'AD':
        """
        returns self * other
        """
        if isinstance(other, float) or isinstance(other, int):
            return AD(self.val * other, other*self.deriv)
        return AD(self.val * other.val, self.val * other.deriv + self.deriv*other.val)
    
    def __rmul__(self, other: float) -> 'AD':
        """
        returns other * self
        """
        return AD(other * self.val, other * self.deriv)

    def __pow__(self, p: int) -> 'AD':
        """
        returns self^p
        """
        if p == -1:
            if (self.deriv <= 0).any():
                raise ValueError("Invalid value of derivative. Must be positive.")
            else:
                return AD(np.power(self.val, p), np.log(self.deriv))
        if p == 0:
            return AD(1., np.zeros_like(self.deriv))
        if p == 1:
            return self
        return AD(np.power(self.val, p), p * (self.val**(p-1)) * self.deriv)

    def __truediv__(self, other: Union[float, 'AD']) -> 'AD':
        """
        returns self / other
        """
        if isinstance(other, float) or isinstance(other, int):
            return AD(self.val / other, self.deriv/other)
        if other.val == 0:
            raise ZeroDivisionError("You can't divide by zero!")
        else: 
            return AD(self.val / other.val, (other.val * self.deriv - self.val * other.deriv) / other.val**2)
    
    def __rtruediv__(self, other: float) -> 'AD':
        """
        returns other / self
        """
        if self.val == 0:
            raise ZeroDivisionError("You can't divide by zero!")
        else:
            return AD(other / self.val, - other * self.deriv / self.val**2)
    

    def __str__(self): # pragma: no cover
        return f"AD({self.val}, {self.deriv})"
    def __repr__(self): # pragma: no cover
        return f"AD({self.val}, {self.deriv})"
    def __eq__(self, other: Union['AD', float]):
        if isinstance(other, float) or isinstance(other, int):
            return np.isclose(self.val, other)
        return np.isclose(self.val, other.val) and np.isclose(self.deriv, other.deriv).all()



